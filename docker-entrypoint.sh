#!/bin/sh

set -e

envsubst '${REGISTRY_USERNAME} ${REGISTRY_PASSWORD}' < /etc/docker/config.yml.tpl > /etc/docker/registry/config.yml

case "$1" in
    *.yaml|*.yml) set -- registry serve "$@" ;;
    serve|garbage-collect|help|-*) set -- registry "$@" ;;
esac

exec "$@"