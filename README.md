# Docker pull through registry

Based on the [official Docker registry image](https://hub.docker.com/_/registry), but configured to run as a pull through (cache) registry with added environment variables to optionally pull through a private dockerhub registry.

#### Getting Started

See:
* [Original image documentation](https://hub.docker.com/_/registry)
* [Pull through registry documentation](https://docs.docker.com/registry/recipes/mirror/)

#### Environment Variables

* `REGISTRY_USERNAME` - Optional dockerhub username
* `REGISTRY_PASSWORD` - Optional dockerhub password

#### Dockerhub image
[https://hub.docker.com/repository/docker/thomasdegraaff/registry-pull-through](https://hub.docker.com/repository/docker/thomasdegraaff/registry-pull-through)
