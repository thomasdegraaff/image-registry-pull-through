FROM registry:2.7.1
RUN apk add --no-cache gettext
COPY config.yml.tpl /etc/docker/config.yml.tpl
COPY docker-entrypoint.sh /entrypoint.sh